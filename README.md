# Ansible Role: Slack Desktop

This role installs [Slack Desktop](https://slack.com/) client on any supported host.

## Requirements

None

## Role Variables

Available variables are listed below, along with default values (see `defaults/main.yml`):

    slack_version: 4.22.0
    slack_package_name: slack-desktop

This role always installs the desired version. See [available Slack Desktop releases](https://slack.com/intl/en-us/downloads/linux).

    slack_version: 4.22.0

The version to the Slack Desktop application.

    slack_package_name: slack-desktop

The name of the package installed in the system (if there need to be updated it).

## Dependencies

None.

## Example Playbook

    - hosts: all
      become: yes
      roles:
        - role: slack-desktop

## License

MIT / BSD
